VulvAPI
=======

A JSON API to fetch vulvas. 

Usage
-----

`/vulvas/n` with n being a natural integer < 0.

Why
---

There is already a JSON [API](https://dicks-api.herokuapp.com) to get dicks. So why not vulvas?!
