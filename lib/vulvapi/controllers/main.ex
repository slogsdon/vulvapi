defmodule Vulvapi.Controllers.Main do
  use Sugar.Controller

  def index(conn, []) do
    raw conn |> resp(200, "Hello world")
  end

  def vulvas(conn, n: id) do
    data = Vulvapi.Generator.produce(id)
    json(conn, data)
  end
end
