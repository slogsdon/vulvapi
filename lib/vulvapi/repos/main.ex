defmodule Vulvapi.Repos.Main do
  use Ecto.Repo,
    adapter: Ecto.Adapters.Postgres,
    otp_app: :vulvapi
end
