defmodule Vulvapi.Generator do

  def produce(i) when i |> is_integer
                  and i > 0 do
    v = "({})"
    list = List.duplicate(v, i)
    %{ "vulvas" => list }
  end
  def produce(s) when s |> is_binary do
    case Integer.parse(s) do
      {i, _} -> produce(i)
      _      -> %{}
    end
  end
  def produce(_), do: %{}

end
