defmodule Router do
  use Sugar.Router

  def run do
    Plug.Adapters.Cowboy.http Router, [], []
  end
  # Uncomment the following line for session store
  # plug Plug.Session, store: :ets, key: "sid", secure: true, table: :session

  # Define your routes here
  get "/", Vulvapi.Controllers.Main, :index
  get "/vulvas/:n", Vulvapi.Controllers.Main, :vulvas
end
